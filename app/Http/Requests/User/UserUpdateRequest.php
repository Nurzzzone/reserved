<?php

namespace App\Http\Requests\User;

use App\Domain\Contracts\UserContract;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UserUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            UserContract::NAME  =>  'required|min:2|max:255',
            UserContract::EMAIL =>  'nullable|unique:users,email,'.$this->id,
            UserContract::PHONE => ['nullable', 'string', 'min:11', 'max:20']
        ];
    }

    public function validated(): array
    {
        $request    =   $this->validator->validated();

        if ($this->has(UserContract::PHONE) AND $this->filled(UserContract::PHONE)) {
            $request[UserContract::PHONE_VERIFIED_AT] = null;
        }

        if ($this->has(UserContract::EMAIL) AND $this->filled(UserContract::EMAIL)) {
            $request[UserContract::EMAIL_VERIFIED_AT] = null;
        }

        return $request;
    }

    protected function failedValidation(Validator $validator)
    {
        $response = [
            'status' => 'failure',
            'status_code' => 400,
            'message' => 'Bad Request',
            'errors' => $validator->errors(),
        ];
        throw new HttpResponseException(response()->json($response, 400));
    }
}
